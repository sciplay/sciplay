class ApiHandler {
    static httpGetAsync(url, callback) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState === 4 && xmlHttp.status === 200)
                callback(xmlHttp.responseText);
        };
        xmlHttp.open("GET", url, true); // true for asynchronous
        xmlHttp.send(null);
    }
}

class QuotesApiHandler extends ApiHandler {
    static randomQuoteUrl = 'https://programming-quotes-api.herokuapp.com/quotes/random';
    static getRandomQuote(callback) {
        ApiHandler.httpGetAsync(QuotesApiHandler.randomQuoteUrl, response => {
            let obj = JSON.parse(response);
            let quote = new Quote(obj.id, obj.en, obj.author);
            callback(quote);
        });
    }
}

class Quote {
    constructor(id, text, author) {
        this.id = id;
        this.text = text;
        this.author = author;
    }

    drawNewQuoteElementIn(parentElement, options = {}) {
        let li = document.createElement("li");
        let quoteNumber = parentElement.childElementCount + 1;

        li.appendChild(document.createTextNode('Quote ' + quoteNumber));
        li.setAttribute("data-id", this.id);
        li.setAttribute("onclick", 'app.setActive(this)');

        if ('removable' in options && options['removable']) {
            let x = document.createElement("span");
            x.appendChild(document.createTextNode('x'));
            x.className = 'remove-btn';
            x.setAttribute("data-id", this.id);
            x.setAttribute("onclick", 'event.stopPropagation(); app.removeFromFavorites(this)');
            li.appendChild(x);
        }

        parentElement.appendChild(li);
    }
}

class QuoteList {
    constructor(elementId, emptyPlaceHolder = '') {
        this.elementId = elementId;
        this.htmlElement = document.getElementById(elementId);
        this.htmlElement.innerHTML = emptyPlaceHolder;
        this.qoutes = [];
    }

    addQuote(quote, options = {}) {
        if (this.contain(quote)) {
            alert('The quote is already exists in the list');
        } else {
            if (this.isEmpty) {
                this.htmlElement.innerHTML = '';
            }
            this.qoutes.push(quote);
            quote.drawNewQuoteElementIn(this.htmlElement, options);
        }
    }

    removeQuote(quoteId) {
        let quote = this.getQuoteById(quoteId);
        if (this.contain(quote)) {
            this.qoutes = this.qoutes.filter(obj => obj.id !== quote.id);
        } else {
            alert('The quote was already removed from the list');
        }
    }

    contain(quote) {
        return this.qoutes.some(obj => obj.id === quote.id);
    }

    get length() {
        return this.qoutes.length;
    }

    get isEmpty() {
        return this.qoutes.length === 0;
    }

    getQuoteById(quoteId) {
        return this.qoutes.find(e => e.id === quoteId);
    }

    highlightQuote(quote) {
        let elm = this.htmlElement.querySelectorAll('[data-id="' + quote.id + '"]');
        let i = 0;
        let intervalID = setInterval(function(){
            if (i % 2 === 0) {
                elm[0].classList.add('highlight');
            } else {
                elm[0].classList.remove('highlight');
            }

            if (++i === 6) {
                window.clearInterval(intervalID);
            }
        }, 180);
    }
}

class StorageHandler {
    static setObject(key, obj) {
        localStorage.removeItem(key);
        localStorage.setItem(key, JSON.stringify(obj));
    }

    static getObject(key) {
        return JSON.parse(localStorage.getItem(key));
    }
}

class App {
    constructor() {
        this.quoteList = new QuoteList('quotes-list-js');
        this.favoriteQuotes = new QuoteList('favorite-quotes-list-js', 'No favorites yet :(');
        this.activeQuote = null;
        this.initFavoritesButton('add-to-favorites-btn');
    }

    loadFavoriteQuotesFromStorage() {
        let favorites = StorageHandler.getObject('favoriteQuotes');
        if (favorites) {
            for (let i = 0; i < favorites.length; i++) {
                let quote = new Quote(favorites[i].id, favorites[i].text, favorites[i].author);
                this.favoriteQuotes.addQuote(quote, {'removable':true});
            }
        }
    }

    initFavoritesButton(elementId) {
        this.favoritesButton = document.getElementById(elementId);
        this.favoritesButton.addEventListener('click', this.addActiveQuoteToFavorites.bind(this))
    }

    addActiveQuoteToFavorites(e) {
        if (this.favoriteQuotes.contain(this.activeQuote)) {
            this.favoriteQuotes.highlightQuote(this.activeQuote);
        } else if (this.favoriteQuotes.length >= 5){
            alert('Favorites can hold up to 5 quotes!');
        } else {
            this.favoriteQuotes.addQuote(this.activeQuote, {'removable': true});
            StorageHandler.setObject('favoriteQuotes', this.favoriteQuotes.qoutes);
            this.favoriteQuotes.highlightQuote(this.activeQuote);
        }
    }

    removeFromFavorites(element) {
        this.setActive(element.parentNode);
        setTimeout(() => {
            if (confirm('Are you sure?')) {
                this.favoriteQuotes.removeQuote(element.dataset.id);
                StorageHandler.setObject('favoriteQuotes', this.favoriteQuotes.qoutes);
                this.favoriteQuotes.htmlElement.innerHTML = '';
                this.favoriteQuotes = new QuoteList('favorite-quotes-list-js', 'No favorites yet :(');
                this.loadFavoriteQuotesFromStorage();
                this.setActive(this.quoteList.qoutes[0]);
            }
        },1);
    }

    setActive(quote) {
        if (quote instanceof Quote) {
            this.activeQuote = quote;
            this.drawInDetailsPanel(quote);
        } else { // treat as html element
            if (this.quoteList.elementId === quote.parentElement.id) {
                quote = this.quoteList.getQuoteById(quote.dataset.id);
            } else if (this.favoriteQuotes.elementId === quote.parentElement.id) {
                quote = this.favoriteQuotes.getQuoteById(quote.dataset.id);
            }
            this.setActive(quote);
        }
    }

    drawInDetailsPanel(quote) {
        document.getElementById('quote-text').innerHTML = quote.text;
        document.getElementById('quote-author').innerHTML = quote.author;
    }

    loadQuoteListFromApi(quotesQuantity) {
        for (let i = 0; i < quotesQuantity; i++) {
            QuotesApiHandler.getRandomQuote((quote) => {
                let setActive = this.quoteList.isEmpty;
                if (this.quoteList.contain(quote)) { // handle duplicated quote
                    this.loadQuoteListFromApi(1);
                } else {
                    this.quoteList.addQuote(quote);
                    if (setActive) {
                        this.setActive(quote);
                    }
                }
            });
        }
    }
}

/*------------------*/

document.addEventListener("DOMContentLoaded", function(event) {
    this.app = new App();
    this.app.loadQuoteListFromApi(10);
    this.app.loadFavoriteQuotesFromStorage();
});
